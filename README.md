# Matrix752
- 此安裝會同時安裝python環境、mysql、apache2、phpmyadmin、執行程式
- ⚠️安裝前需知
    - 若mysql已安裝，需要有`root/root`存在，且有建立資料庫的權限
    - 無安裝mysql，此安裝腳本會同時安裝mysql，若在安裝過程中出現帳密設定就在此設定`root/root`
    - 安裝時，需要連上外網確認此裝置是被認證過的
## 安裝步驟
1. 以root登入matrix752
2. 下載[壓縮檔](http://gofile.me/3bqcl/LfwiKM94S)到`/root`目錄下
3. 解壓後進入matrix752_install目錄
    ```bash=
    unzip matrix752_install.zip
    cd matrix752_install
    ```
4. 執行腳本
    ```bash=
    sh install.sh
    ```
## 使用
- 在瀏覽器上輸入設備IP，進行485設備參數設定
- 提供一般使用者一組帳密(read only)，可訪問ig-node下的history_data、channel_info、device_info、temp資料表
    - `user1 / rc3Tnd6U`